/* (C) Copyright 2013  CERN

   This software is distributed under the terms of the GNU General Public
   License version 3 (GPL Version 3), copied verbatim in the file "COPYING".

   In applying this license, CERN does not waive the privileges and immunities
   granted to it by virtue of its status as an Intergovernmental Organization
   or submit itself to any jurisdiction.

 */

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class launcher;
#pragma link C++ class TVSDReader;
#pragma link C++ class LbMclassTrack+;
#pragma link C++ class Frame;



#endif
